# -*- coding: utf-8; mode: org-development-lisp; -*-
#+title: “Package file” for serere system
#+subtitle: part of the =serere= Common Lisp system
#+property: header-args :package common-lisp
#+author: =#<PERSON "akater" A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>=

* serere
#+begin_src lisp :tangle yes :results none
(cl:defpackage #:serere
  (:nicknames ;; #:sow-reap #:reap-sow
              )
  (:use #:cl)
  (:shadow #:hash-table)
  (:export #:hash-table #:alist

           #:*environment* #:+none+
           
           #:reap #:sow #:sow* #:reaped #:tag

           #:reap*
           #:reaps

           #:reap-into #:reap-fold

           #:areap #:areap*

           #:reap-array #:reap-hash-table #:reap-alist))
#+end_src
