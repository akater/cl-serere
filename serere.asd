;;;; serere.asd

;; (cl:eval-when (:load-toplevel :execute)
;;    #+quicklisp (ql:quickload :literate-lisp)
;;    #-quicklisp (asdf:load-system :literate-lisp))

;; Workaround for literate-lisp-related bug, see
;; https://github.com/jingtaozf/literate-lisp/issues/14

#+(and sbcl swank) (in-package #:swank/source-path-parser)
#+(and sbcl swank) (when (sb-int:encapsulated-p 'guess-reader-state
                                                :literate-lisp)
                     (sb-int:unencapsulate 'guess-reader-state :literate-lisp))
#+(and sbcl swank) (sb-int:encapsulate
                    'guess-reader-state :literate-lisp
                    (lambda (orig-func stream)
                      (multiple-value-bind (readtable package) (funcall
                                                                orig-func
                                                                stream)
                        (let ((point (file-position stream)))
                          (file-position stream 0)
                          (let ((line (read-line stream nil nil)))
                            (file-position stream point)
                            (when (and line (starts-with-p line "# "))
                              (setq readtable (copy-readtable readtable))
                              (let ((*readtable* readtable))
                                (literate-lisp:install-globally))))
                          (values readtable package)))))

(asdf:defsystem #:serere
  :description "A DSL for collecting values."
  :author "=#<PERSON akater A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>="
  :license "UNLICENSE"
  :version "20230808"
  :serial t
  :defsystem-depends-on (#:literate-lisp)

  :around-compile (lambda (next)
                    (proclaim '(optimize
				(compilation-speed 0)
                                (debug 3)
                                (safety 3)
                                (space 0)
                                (speed 0)))
                    (funcall next))

  :components ((:org "packages")
               (:org "serere-helpers")
	       (:org "serere")))

