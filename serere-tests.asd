;;;; serere-tests.asd

;; (cl:eval-when (:load-toplevel :execute)
;;    #+quicklisp (ql:quickload :literate-lisp)
;;    #-quicklisp (asdf:load-system :literate-lisp))

(asdf:defsystem #:serere-tests
  :description "Tests and Examples for serere."
  :author "=#<PERSON akater A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>="
  :license "UNLICENSE"
  ;; :version "20200907"
  :serial t
  :depends-on (#:literate-lisp #:serere #:cl-num-utils)

  :around-compile (lambda (next)
                    (proclaim '(optimize 
				(compilation-speed 0)
                                (debug 3)
                                (safety 3)
                                (space 0)
                                (speed 0)))
                    (funcall next))

  :components ((:org "serere-tests-helpers")
               (:org "serere-tests")))
